/*
* This file contains any information which needs to be stored in a secure place
* Also it is good because you don't have to change the value everywhere you have used them
* By just changing values here, it will make your life a lot easier
*
* Returns different object depends on the current environment setup
* 
* @author Ken Huh <ken123777@gmail.com>
*/

module.exports = function(env){
  switch(env){
    case "development":
      return {
        db: 'mongodb://127.0.0.1:27017/devDirectors-api',
        livestream_uri: 'https://api.new.livestream.com/accounts/'
      };
      break;
    case "test":
      return {
        db: 'mongodb://127.0.0.1:27017/testDirectors-api',
        livestream_uri: 'https://api.new.livestream.com/accounts/'
      };
      break;
  }
};

/*
* This module contains environments setup for express.js app
* Server runs this file on initial booting
*
* Check server.js to find the place where exactly this module is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

var express = require('express');

// all environments
module.exports = function(app){
  app.set('port', process.env.PORT || 3000);
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(function(err, req, res, next){
    console.error(err);
    res.send(500, { 'Error': err });
  });
};

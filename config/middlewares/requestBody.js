/*
* Handles request body verification for necessary attributes
* Router calls this middleware to see if there is any missing attributes in the request body
*
* Check config/routes.js to find the place where exactly this middleware is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

// check for livestream_id
exports.checkLivestreamId = function(req, res, next){
  var livestreamId = req.body.livestream_id;
  if (!livestreamId) {
    res.send(400, { 'Error': 'Missing Livestream ID in request body' });
  } else {
    next();
  }
};

// check for favorite_camera && favorite_movies
exports.checkFavoriteCameraAndMovies = function(req, res, next){
  if (!req.body.favorite_camera && !req.body.favorite_movies) {
    // if all parameters are missing throw error
    res.send(400, { 'Error': 'At least one of following attributes is needed to update - "favorite_movies", "favorite_camera"' });
  }
};

// check for favorite_movies
exports.checkFavoriteMovies = function(req, res, next){
  if (!req.body.favorite_movies) {
    res.send(400, { 'Error': 'Please specify "favorite_movies"'});
  }
};

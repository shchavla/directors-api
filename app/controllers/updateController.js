/*
* Handles logic for directors accouts update
* Router calls for this module when directors make requests to update their camera/movies
*
* Check config/routes.js to find the place where exactly this controller is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

var mongoose = require('mongoose');
var Director = mongoose.model('Director');

// run reset when a director makes PUT request to replace all camera/movies values
exports.reset = function(req, res){
  var livestreamId = req.params.livestream_id;
  var favoriteCamera = req.body.favorite_camera || "";
  var favoriteMovies = req.body.favorite_movies || [];

  Director.findOne({ livestream_id: livestreamId }, {}, { '_id': false, '__v': false }, function(err, director){
    if (err) {
      console.error(err);
      res.send(400, { 'Error': 'Invalid Livestream ID number' });
    } else {
      director.favorite_camera = favoriteCamera;
      director.favorite_movies = favoriteMovies;
      director.save(function(error){
        if (error) {
          console.error(error);
          res.send(500, { 'Error': 'Something went wrong' });
        } else {
          res.send(200, director);          
        }
      });
    }
  });
};

// run addMovie when a director makes POST request to add a movie to his favorite_movies
exports.addMovie = function(req, res){
  var livestreamId = req.params.livestream_id;
  var favoriteMovie = req.body.favorite_movies;

  Director.findOne({ livestream_id: livestreamId }, function(error, director){

    // check if the movie already exists in the favorite_movies
    director.favorite_movies.forEach(function(movie){
      if (movie === favoriteMovie) {
        res.send(400, { 'Error': 'Movie already exists' });
      }
    });

    // push the movie to the favorite_movies array
    director.update({ $push: { 'favorite_movies' : favoriteMovie } }, function(err){
      if (err) {
        console.error(err);
        res.send(500, { 'Error': 'Something went wrong' });
      } else {
        res.send(200, { 'Success': 'Updated successfully' });
      }
    });
  });
};

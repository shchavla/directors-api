/*
* Handles logic for listing all directors accouts
* Router calls for this module when anyone makes a request to see all registered directors
*
* Check config/routes.js to find the place where exactly this controller is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

var mongoose = require('mongoose');
var Director = mongoose.model('Director');

// handles listing registered directors
exports.index = function(req, res){
  Director.find({}, {'_id': false, '__v': false}, function(err, directors){
    if (err) {
      console.error(err);
      res.send(500, { 'Error': 'Something went wrong' });
    } else {
      res.send(200, directors);
    }
  });
};

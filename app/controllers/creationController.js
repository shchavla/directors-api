/*
* Handles logic for directors accout creation
* Router calls for this module when directors make requests to register on my API platform
*
* Check config/routes.js to find the place where exactly this controller is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

var request = require('request');
var mongoose = require('mongoose');
var Director = mongoose.model('Director');
var env = process.env.NODE_ENV || 'development';
var credentials = require('../../config/credentials')(env);

// handles director registration
exports.register = function(req, res){
  var livestreamId = req.body.livestream_id;
  var options = { host: credentials.livestream_uri + livestreamId };

  // Since favorite_camera and favorite_movies are optional,
  // set them to empty values if director did not pass those in
  var favoriteCamera = req.body.favorite_camera || "";
  var favoriteMovies = req.body.favorite_movies || [];

  request(options.host, function(error, response, body){

    // if successfully received the response from Livestream accounts API
    if (!error && response.statusCode === 200) {
      body = JSON.parse(body);

      // use a helper function which is defined at the bottom to save to the database
      saveDirectorToDB(res, livestreamId, body, favoriteCamera, favoriteMovies, res);

    // if there was an error communicating with Livestream accouts API
    } else {
      console.error(error);
      res.send(500, { 'Error': 'Something went wrong' });
    }
  });
};

// a helper function to save directors to the database
var saveDirectorToDB = function(res, livestreamId, body, favoriteCamera, favoriteMovies){
  Director.findOrCreate({ livestream_id: livestreamId }, function(err, director, created){
    if (created) {
      director.favorite_movies = favoriteMovies;
      director.favorite_camera = favoriteCamera;
      director.dob = body.dob;
      director.full_name = body.full_name;
      director.save(function(error){
        if (error) {
          console.error(error);
          res.send(500, { 'Error': 'Something went wrong' });
        } else {
          res.send(200, {
            livestream_id: director.livestream_id,
            full_name: director.full_name,
            dob: director.dob,
            favorite_camera: director.favorite_camera,
            favorite_movies: director.favorite_movies
          });
        }
      });

    // does not allow duplicate Livestream ID numbers
    } else {
      res.send(403, { Error: "Livestream ID already exists"});
    }
  });
};

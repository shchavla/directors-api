Livestream-directors-api
========================

Node.js Directors API for Livestream

##Contents

- [Running node server](#running-node-server)
- [API Guide](#api-guide)
- [Motivation](#motivation)

##Running node server

  Clone the project:

     $ git clone https://github.com/Ken123777/Livestream-directors-api.git

  cd into the project folder:

     $ cd Livestream-directors-api
  
  Install all dependencies:

    $ npm install

  Run MongoDB server in a new terminal window or tab:

    $ mongod

  Start the server from project root directory:

    $ node server.js

  Start test from project root directory:

    $ npm test


##API Guide

#### All input and response data should be JSON data

##### GET `/directors`

Returns an object of all registered directors on platform

##### POST `/directors`

Request Object: livestream_id(required), favorite_camera(optional), favorite_movies(optional array)

Response Object: An object of new director

##### PUT `/directors/:livestream_id`

Request Object (At least one field is required): favorite_camera, favorite_movies(an array)

Response Object: An object of updated director object

##### POST `/directors/:livestream_id/favorite_movies`

Request Object: favorite_movies(required string)

Response Object: An object with a success message upon update

var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');
var superagent = require('superagent');
var server = require('../server');


var url = 'http://127.0.0.1:3000/directors';
var db = 'mongodb://127.0.0.1/devDirectors-api';

var valid = {
    "livestream_id": "6488818"
};

var valid2 = {
    "livestream_id": "6488828"
};


//EXPERIMENTALLY FOUND N\E ID
var invalid = {
    "livestream_id": "6488829"
};
var empty = {};


var update = {
    favorite_camera: 'CAM1',
    favorite_movies: ['MOV1']
};


var validator = [{
    "dob": "1942-11-17T00:00:00.000Z",
    "favorite_camera": "",
    "favorite_movies": [],
    "full_name": "Martin Scorsese",
    "livestream_id": 6488818
}, {
    "dob": null,
    "favorite_camera": "",
    "favorite_movies": [],
    "full_name": "Jordan Loven",
    "livestream_id": 6488828
}];

describe('directors-api: create account', function () {

    var request = superagent.agent();


    it('should return account data when registers director with valid ID', function (done) {



        request
            .post(url)
            .send(valid)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(200);
                res.body.livestream_id.should.eql(6488818);
                res.body.full_name.should.equal("Martin Scorsese");
                res.body.dob.should.eql('1942-11-17T00:00:00.000Z');
                res.body.favorite_camera.should.eql('');
                res.body.favorite_movies.should.eql([]);
                done();
            });
    });


    it('should return error on re-register same account ID', function (done) {
        request
            .post(url)
            .send(valid)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(200)



                request
                    .post(url)
                    .send(valid)
                    .end(function (err, res) {
                        if (err) {
                            throw err;
                        }
                        res.status.should.eql(403)
                        res.body.should.not.be.empty; // REASON for denial SHOULD BE listed
                        done();
                    });
            });
    });



    it('should return error when empty payload', function (done) {
        request
            .post(url)
            .send(empty)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(400);
                done();
            });
    });


    //TRICKY ONE: currently call returns 500 but I think it should return (403) because invalid\non existing livestream ID - is know scenario;
    it('should return proper (403) error when register with invalid livestream ID', function (done) {
        request
            .post(url)
            .send(invalid)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(403)
                res.body.should.not.be.empty; // REASON for denial SHOULD BE listed
                done();
            });
    });


});


describe('directors-api: update account', function () {

    var request = superagent.agent();


    it('should fail on update without valid authorization parameter', function (done) {
        request
            .post(url)
            .send(valid)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(200);

                request
                    .put(url + '/' + valid.livestream_id)
                    .send(update)
                    .end(function (err, res) {
                        if (err) {
                            throw err;
                        }
                        res.status.should.eql(401);
                        res.body.should.not.be.empty;
                        done();
                    });
            });
    });
});



describe('directors-api: list accounts', function () {

    var request = superagent.agent();

    beforeEach(function (done) {

        request
            .post(url)
            .send(valid)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(200);
                request
                    .post(url)
                    .send(valid2)
                    .end(function (err, res) {
                        if (err) {
                            throw err;
                        }
                        res.status.should.eql(200);
                        done();
                    });
            });
    });




    it('should list all regirtered accounts', function (done) {

        request
            .get(url)
            .end(function (err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.eql(200);

                res.body[0].livestream_id.should.eql('6488818');
                res.body[0].full_name.should.eql('Martin Scorsese');
                res.body[1].livestream_id.should.eql('6488828');
                res.body[1].full_name.should.eql('Jordan Loven');
                done();
            });

    });


});




//DB REFRESH
beforeEach(function (done) {
    var dropData = function () {
        for (var i in mongoose.connection.collections) {
            mongoose.connection.collections[i].remove(function () {});
        }
        return done();
    };

    if (mongoose.connection.readyState === 0) {
        mongoose.connect(db, function (err) {
            return dropData();
        });
    } else {
        return dropData();
    }
});

afterEach(function (done) {
    mongoose.disconnect();
    return done();
});

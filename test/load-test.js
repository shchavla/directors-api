
var loadtest = require('loadtest');

var options = {
    url: 'http://127.0.0.1:3000/directors',
    maxRequests: 10000,
    concurenncy: 100,
};

loadtest.loadTest(options, function(error, result)
{
    if (error)
    {
        return console.error('Got an error: %s', error);
    }
    console.log('Load test completed');
    console.log(result);
});
